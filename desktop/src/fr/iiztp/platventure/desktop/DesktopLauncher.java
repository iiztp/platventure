package fr.iiztp.platventure.desktop;

import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Application;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3ApplicationConfiguration;
import fr.iiztp.platventure.PlatVenture;

public class DesktopLauncher {
	public static void main (String[] arg) {
		Lwjgl3ApplicationConfiguration config = new Lwjgl3ApplicationConfiguration();
		config.setTitle("PlatVenture");
		config.setWindowedMode(1000, 600);
		new Lwjgl3Application(new PlatVenture(), config);
	}
}
